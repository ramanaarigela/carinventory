import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AjaxCallService } from '../ajaxcall.service';

@Component({
  selector: 'app-carmodel',
  templateUrl: './carmodel.component.html',
  styleUrls: ['./carmodel.component.css']
})
export class CarModelComponent implements OnInit {
  manufactarr = [];
  imgdummy1:string='';
  imgdummy2:string='';
  file1:File;
  file2:File;
  successmsg = false;
  carmodelobj = {
      name:'',
      manfid:0,
      color:'',
      regnum:'',
      manfyear:2019,
      notes:'',
      count:1,
      img1:'',
      img2:''
  }
  validations = {
    name:false,
    manfid:false,
    color:false,
    regnum:false,
    manfyear:false,
      
}

 @ViewChild('imageInput1') inputfile1 :ElementRef;
 @ViewChild('imageInput2') inputfile2 :ElementRef;
  constructor(public ajaxcall:AjaxCallService ) { 
    this.getmanufacturers();
  }

  ngOnInit() {
  }
  getmanufacturers()
  {
    this.ajaxcall.getdata('manufacturer.php',JSON.stringify({action:'readmanufacturers',manfid:0}))
    .subscribe((response)=> {
                                this.manufactarr = JSON.parse(response['_body']);
                            },
               (error)=> console.log(error));
  }
  
  processFile(imageInput: any,param:number) {
    const file: File = imageInput.files[0];
    this['file'+param] = file;
    if(file == undefined)
    {
        this['imgdummy'+param] = '';
    }
    else
    {
        const reader = new FileReader();
        reader.addEventListener('load', (event: any) => {
            this['imgdummy'+param] = event.target.result;
           
        });
        reader.readAsDataURL(file);
    }
    
    
  }
  insertconfirm()
  {
    
    this.ajaxcall.getdata('carmodel.php',JSON.stringify({action:'addcarmodel',modeldata:this.carmodelobj}))
                    .subscribe((response)=> {
                            if(response['_body'] == 'true')
                            {
                            this.carmodelobj = {
                                name:'',   manfid:0,   color:'',   regnum:'',  manfyear:2019,
                                notes:'',  count:1,   img1:'',    img2:''
                            }
                            this.imgdummy1 = '', this.imgdummy2='';
                            this.inputfile1.nativeElement.value = '';
                            this.inputfile2.nativeElement.value = '';
                            this.successmsg = true;
                            setTimeout(()=>this.successmsg = false , 2000 );
                            }
                        },
            (error)=> console.log(error));
  }
  insertcarmodel()
  {
    this.validations.name = false , this.validations.manfid = false ;
    if(this.carmodelobj.name == '')
    {
        this.validations.name = true;
    }
    if(this.carmodelobj.manfid == 0)
    {
        this.validations.manfid = true;
    }
    if(this.validations.name == false && this.validations.manfid == false)
    {
        if(this.imgdummy1.length > 0)
        {
            this.ajaxcall.uploadImage(this.file1)
                .subscribe((response)=> {
                                            
                                            this.carmodelobj.img1 = response['_body'];
                                            if(this.imgdummy2.length > 0)
                                            {
                                                this.ajaxcall.uploadImage(this.file2)
                                                    .subscribe((response)=> {
                                                    
                                                        this.carmodelobj.img2 = response['_body'];
                                                        this.insertconfirm();
                                                });
                                            }
                                            else{
                                                this.insertconfirm();
                                            }
                                        });
        }
        else{
            this.insertconfirm();
        }
    }
    
  }

}
