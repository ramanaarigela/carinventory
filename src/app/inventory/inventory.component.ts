import { Component, OnInit, TemplateRef } from '@angular/core';
import { AjaxCallService } from '../ajaxcall.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {
  carmodels = [];
  carmodelDetails = {
    id:0 , name:'',   manfname:0,   color:'',   regnum:'',  manfyear:0,
    notes:'',  count:1,   img1:'',    img2:''
}
  modalRef: BsModalRef;
  constructor(public ajaxcall:AjaxCallService ,private modalService: BsModalService) { 
    
  }

  ngOnInit() {
    this.getmanufacturers();
  }
  
   
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  getmanufacturers()
  {
    this.ajaxcall.getdata('carmodel.php',JSON.stringify({action:'readcarmodels'}))
    .subscribe((response)=> {
                                this.carmodels = JSON.parse(response['_body']);
                                console.log(this.carmodels);
                            },
               (error)=> console.log(error));
  }
  getdetails(param,template: TemplateRef<any>)
  {
      console.log(this.carmodels[param]);
      this.modalRef = this.modalService.show(template,{class: 'modal-lg'});
      this.carmodelDetails = {
        id:this.carmodels[param].id, name:this.carmodels[param].name,   manfname:this.carmodels[param].manfname,   color:this.carmodels[param].color, 
        regnum:this.carmodels[param].regnum,  manfyear:this.carmodels[param].manufyear, notes:this.carmodels[param].note, 
        count:this.carmodels[param].quntity,  
        img1:this.ajaxcall.imgpath+this.carmodels[param].img1,    img2:this.ajaxcall.imgpath+this.carmodels[param].img2
    }
  }
  soldmodel(modelid)
  {
    this.ajaxcall.getdata('carmodel.php',JSON.stringify({action:'soldmodel',modelid:modelid}))
    .subscribe((response)=> {
                                if(response['_body'] == 'true')
                                {
                                    this.carmodels.splice(this.carmodels.findIndex(x=> x.id = modelid), 1);
                                    this.modalRef.hide();
                                }
                            },
               (error)=> console.log(error));
  }
}
