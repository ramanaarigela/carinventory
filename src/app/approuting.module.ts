import { NgModule } from "@angular/core";
import {Routes,RouterModule} from '@angular/router';
import { ManufacturerComponent } from "./manufacturer/manufacturer.component";
import { CarModelComponent } from "./carmodel/carmodel.component";
import { InventoryComponent } from "./inventory/inventory.component";
const approutes:Routes = [
    {
        path:'',component:ManufacturerComponent
    },
    {
        path:'manufacturer',component:ManufacturerComponent
    },
    {
        path:'carmodel',component:CarModelComponent
    },
    {
        path:'inventory',component:InventoryComponent
    }

    
]
@NgModule({
    imports: [RouterModule.forRoot(approutes)],
    exports:[RouterModule]
})
export class AppRoutingModule{}