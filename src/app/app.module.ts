import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ManufacturerComponent } from './manufacturer/manufacturer.component';
import { AppRoutingModule } from './approuting.module';
import { CarModelComponent } from './carmodel/carmodel.component';
import { AjaxCallService } from './ajaxcall.service'
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { InventoryComponent } from './inventory/inventory.component';
import {DataTableModule} from 'angular-6-datatable';
@NgModule({
  declarations: [
    AppComponent,
    ManufacturerComponent,
    CarModelComponent,
    InventoryComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    AppRoutingModule,
    DataTableModule
    
  ],
  providers: [AjaxCallService],
  bootstrap: [AppComponent]
})
export class AppModule { }
