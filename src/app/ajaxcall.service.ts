import { Injectable } from "@angular/core";
import {Http,Response} from "@angular/http"
import { Observable } from "../../node_modules/rxjs";
import 'rxjs';
@Injectable()
export class AjaxCallService{

    server = 'http://localhost/carinventory/api/';
    imgpath = 'http://localhost/carinventory/images/';
    // server = 'https://aimchassis.com/testsite/';
    constructor(public http:Http)
    {

    }

    getdata(url,paramobj)
    {
        return this.http.post(this.server+url    ,   paramobj);
    }
    uploadImage(image: File): Observable<Response> {
        const formData = new FormData();
        formData.append('file', image);
        return this.http.post(this.server+'fileupload.php', formData);
      }
}