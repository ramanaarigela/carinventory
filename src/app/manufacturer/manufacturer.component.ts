import { Component, OnInit } from '@angular/core';
import { AjaxCallService } from '../ajaxcall.service';

@Component({
  selector: 'app-manufacturer',
  templateUrl: './manufacturer.component.html',
  styleUrls: ['./manufacturer.component.css']
})
export class ManufacturerComponent implements OnInit {
  mname:string='';
  successmsg = false;
  validmsg = false;
  
  constructor(public ajaxcall:AjaxCallService) { }

  ngOnInit() {
  }


  addmanufacturer(event:Event)
  {
      if(this.mname.length == 0)
      {
        this.validmsg = true;
      }
      else
      {
        this.validmsg = false;
        this.ajaxcall.getdata('manufacturer.php',JSON.stringify({action:'addmanufacturer',manufacturer:this.mname}))
                 .subscribe((response)=> 
                                        {
                                          // console.log(response);
                                          if(response['_body'] == 'true')
                                          {
                                            this.mname = '';
                                            this.successmsg = true;
                                            setTimeout(()=>this.successmsg = false , 2000 );
                                          }
                                          
                                        },
                            (error)=> console.log(error));
        
      }
  }
}
